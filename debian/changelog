python-path-and-address (2.0.1-4) unstable; urgency=medium

  * Team upload.

  [ Carl Suster ]
  * d/watch: track tags rather than empty releases page.

  [ Colin Watson ]
  * Use dh-sequence-python3.
  * Use pybuild-plugin-pyproject.
  * Enable autopkgtest-pkg-pybuild.

 -- Colin Watson <cjwatson@debian.org>  Wed, 12 Feb 2025 12:11:33 +0000

python-path-and-address (2.0.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:14:34 -0400

python-path-and-address (2.0.1-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Thu, 25 Jul 2019 20:06:29 +0200

python-path-and-address (2.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Set DPMT as Maintainer, add myself to Uploaders.
    - Bump Standards-Version to 3.9.8 (no changes needed).
    - Remove dot from short description.
    - Update 'Vcs-*' fields from 'collab-maint' to 'python-modules' namespace.
  * debian/copyright: change license name from 'MIT' to 'Expat', as the first
    one isn't a valid short name per DEP-5.
  * Switches from 'gbp' to 'git-dpm' (as required by DPMT).

 -- Tiago Ilieve <tiago.myhro@gmail.com>  Thu, 21 Jul 2016 02:47:22 -0300

python-path-and-address (1.1.0-1) unstable; urgency=medium

  * Initial release (Closes: #790612)

 -- Tiago Ilieve <tiago.myhro@gmail.com>  Sat, 02 Apr 2016 00:36:32 -0300
